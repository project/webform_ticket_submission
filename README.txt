Introduction
------------

Module was originally designed to submit 'Issues' to Github from webform
submissions. It has been built however to be extended so other services can be
used, ie; Jira.

Requirements
------------

This module requires the following module:

Webform 4.x (Has not been tested for Webform 3.x but should be fine.)

Installation
------------

Copy webform_ticket_submission to your module directory and then enable on
the admin modules page.
Enable the github_ticket module as well.

Configuration
-------------

Settings for each individual webform can be found at
node/%webform_menu/webform/ticket_submission, where % is the webform $nid.

Author
------
Glen Ranson
