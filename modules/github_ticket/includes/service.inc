<?php

/**
 * @file
 * Provides Github ticket with implementation into webform ticket submission.
 */

/**
 * Class GithubTicketService.
 */
class GithubTicketService extends WebformTicketSubmissionAbstractService {

  /**
   * {@inheritdoc}
   */
  public function configurationForm(array $form, array &$form_state, $values) {
    form_load_include($form_state, 'inc', 'webform', 'includes/webform.components');

    $settings = unserialize($values['options']);

    $form['AccessToken'] = array(
      '#type' => 'textfield',
      '#title' => t('Access Token'),
      '#description' => t('Enter the Access Token from Github.'),
      '#required' => TRUE,
      '#default_value' => isset($settings['AccessToken']) ? $settings['AccessToken'] : '' ,
    );

    $form['User'] = array(
      '#type' => 'textfield',
      '#title' => t('Github Username'),
      '#description' => t('Enter the associated Github user name.'),
      '#required' => TRUE,
      '#default_value' => isset($settings['User']) ? $settings['User'] : '',
    );

    $form['Repos'] = array(
      '#type' => 'textfield',
      '#title' => t('Repository'),
      '#description' => t('The Repository name issues will be created against.'),
      '#required' => TRUE,
      '#default_value' => isset($settings['Repos']) ? $settings['Repos'] : '',
    );

    $form['GithubURL'] = array(
      '#type' => 'textfield',
      '#title' => t('Github URL'),
      '#description' => t('Github URL to connect to.'),
      '#required' => TRUE,
      '#default_value' => isset($settings['GithubURL']) ? $settings['GithubURL'] : '',
    );

    $form['Labels'] = array(
      '#type' => 'textfield',
      '#title' => t('Associated Labels'),
      '#description' => t('Seperate the different Labels with a comma and No spaces.'),
      '#default_value' => isset($settings['Labels']) ? $settings['Labels'] : '',
    );

    $form['title_options'] = array(
      '#type' => 'radios',
      '#title' => t('Ticket title options'),
      '#description' => t('Choose which option to use as the title'),
      '#options' => array(
        'text' => t('Type title'),
        'component' => t('Choose webform component'),
      ),
      '#default_value' => isset($settings['title_options']) ? $settings['title_options'] : '',
    );

    $form['title_default'] = array(
      '#type' => 'textfield',
      '#title' => t('Custom title'),
      '#states' => array(
        'visible' => array(
          ':input[name="options[form][title_options]"]' => array('value' => 'text'),
        ),
      ),
      '#default_value' => isset($settings['title_default']) ? $settings['title_default'] : '',
    );

    $form['title_input'] = array(
      '#type' => 'select',
      '#title' => t('Webform component for title'),
      '#options' => webform_component_list($this->node, NULL, FALSE),
      '#empty_option' => t('- Select Component -'),
      '#states' => array(
        'visible' => array(
          ':input[name="options[form][title_options]"]' => array('value' => 'component'),
        ),
      ),
      '#default_value' => isset($settings['title_input']) ? $settings['title_input'] : '',
    );

    $form['body'] = array(
      '#type' => 'textarea',
      '#title' => t('Ticket body'),
      '#default_value' => isset($settings['body']) ? $settings['body'] : '',
      '#description' => theme('webform_token_help', array('groups' => array('node', 'submission'))),
    );

    $form['components'] = array(
      '#type' => 'select',
      '#title' => t('Included webform values'),
      '#options' => webform_component_list($this->node, 'email', TRUE),
      '#default_value' => array_keys($this->node->webform['components']),
      '#description' => t('The selected components will be included in the [submission:values] token. Individual values may still be printed if explicitly specified as a [submission:values:?] in the template.'),
      '#process' => array('webform_component_select'),
    );

    $form['log_requests'] = array(
      '#type' => 'checkbox',
      '#title' => t('Log requests to Github'),
      '#description' => t('Use for debugging purposes to see response coming back from Github as watchdog'),
      '#default_value' => isset($settings['log_requests']) ? $settings['log_requests'] : '',
    );

    return $form;
  }

  /**
   * Helper function to return token values.
   *
   * @param string $input
   *   Input from form.
   * @param object $submission
   *   Webform submission values.
   *
   * @return mixed
   *   Values from tokens.
   */
  public function getTokenValues($input, $submission) {

    $token_data = array();
    if ($this->node) {
      $token_data['node'] = $this->node;
    }
    if ($submission) {
      $token_data['webform-submission'] = $submission;
    }

    $token_results = token_replace($input['body'], $token_data);

    // Replace tokens with actual submission data.
    return $token_results;
  }

  /**
   * Submission handler to POST to Github.
   *
   * @param object $submission
   *   Webform submission.
   * @param array $settings
   *   Form settings.
   */
  public function submitTicket($submission, array $settings) {

    $options = unserialize($settings['options']);

    // If settings for title are to use a custom typed entry.
    if (isset($options['title_options']) && $options['title_options'] == 'text') {
      $title = $options['title_default'];
    }
    else {
      // If settings for title are to use a webform component value.
      $title = $options['title_input'];
      // All components in submission.
      $components = $submission->data;
      foreach ($components as $key => $value) {
        if ($title == $key) {
          $title = (array_shift($value));
        }
      }
    }

    $token = $options['AccessToken'];
    $url = $options['GithubURL'];
    $username = $options['User'];
    $repos = $options['Repos'];
    $labels = explode(',', trim($options['Labels']));

    $json = array(
      'title' => $title,
      'body' => $this->getTokenValues($options, $submission),
      'labels' => $labels,
    );

    $json = drupal_json_encode($json);

    $post = array(
      'method' => 'POST',
      'data' => $json,
      'headers' => array(
        'Content-Type' => 'application/json',
        'Authorization' => 'token ' . $token,
      ),
    );

    $output = drupal_http_request($url . '/' . $username . '/' . $repos . '/issues', $post);
    // Watchdog for attempt to send failed likely due network issues.
    if (isset($output->error) && !isset($output->status_message)) {
      watchdog('github_ticket', "<pre>$output->error</pre>", array(), WATCHDOG_ERROR);
    }
    // Watchdog for posting a ticket passes.
    elseif ($options['log_requests'] && $output->code == 201) {
      watchdog('github_ticket', "<pre>Status : @status <br> Request : @request", array('@status' => $output->status_message, '@request' => $output->request), WATCHDOG_NOTICE);
    }
    // Watchdog for posting a ticket fails.
    else {
      watchdog('github_ticket', "<pre>Status : @status <br> Request : @request", array('@status' => $output->status_message, '@request' => $output->request), WATCHDOG_ERROR);
    }
  }

}
