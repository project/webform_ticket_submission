<?php

/**
 * @file
 * Defines webform ticket submission form.
 */

/**
 * Page callback for webform ticket submission settings form.
 */
function webform_ticket_submission_settings($form, &$form_state, $node) {

  // Check the whether or not the current settings are disabled or not.
  $result = db_select('webform_ticket_submission', 'w')
    ->fields('w')
    ->condition('nid', $node->nid, '=')
    ->execute()
    ->fetchAssoc();

  // If ajax has run use the submitted class value.
  if (!empty($form_state['values']['service_option'])) {
    $class = $form_state['values']['service_option'];
  }
  // On page load before ajax.
  elseif (!empty($result['machinename']) && !isset($form_state['values'])) {
    $class = $result['machinename'];
  }
  else {
    $class = '';
  }

  // Store the nid.
  $form_state['nid'] = $node->nid;

  $form['settings_disable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable settings for this webform'),
    '#default_value' => isset($result['disable']) ? $result['disable'] : 0,
  );

  $form['settings_container'] = array(
    '#type' => 'container',
    '#states' => array(
      'visible' => array(
        ':input[name="settings_disable"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['settings_container']['service_option'] = array(
    '#type' => 'select',
    '#title' => t('Service'),
    '#options' => array(),
    '#empty_option' => t('- Select service -'),
    '#ajax' => array(
      'callback' => 'webform_ticket_submission_add_service_ajax_callback',
      'wrapper' => 'webfrom-ticket-submission-class-options',
    ),
    '#default_value' => isset($result['machinename']) ? $result['machinename'] : '',
  );

  $services = webform_ticket_submission_get_services();

  if (empty($services)) {
    drupal_set_message(t('No services are available.'), 'warning');
  }
  // Populate the service configuration form.
  foreach ($services as $id => $info) {

    if (empty($form_state['storage'])) {
      $form['settings_container']['service_option']['#options'][$id] = $info['name'];
    }

    if (!$class || $class != $id) {
      continue;
    }

    $service = NULL;
    if (class_exists($info['class'])) {
      $service = new $info['class']();
    }

    if (!($service instanceof WebformTicketSubmissionInterface)) {
      watchdog('Webform_ticket_submission', 'Service class @id specifies an illegal class: @class', array(
        '@id' => $id,
        '@class' => $info['class'],
      ), NULL, WATCHDOG_ERROR);
      continue;
    }

    $service->node = $node;
    $service_form = isset($form['settings_container']['options']['form']) ? $form['settings_container']['options']['form'] : array();
    $service_form = $service->configurationForm($service_form, $form_state, $result);

    $form['settings_container']['options']['form'] = $service_form ? $service_form : array('#markup' => t('There are no configuration options for this service class.'));
    $form['settings_container']['options']['service_option']['#type'] = 'value';
    $form['settings_container']['options']['service_option']['#value'] = $class;
    $form['settings_container']['options']['#type'] = 'fieldset';
    $form['settings_container']['options']['#tree'] = TRUE;
    $form['settings_container']['options']['#collapsible'] = TRUE;
    $form['settings_container']['options']['#title'] = $info['name'];
    $form['settings_container']['options']['#access'] = TRUE;
    $form['settings_container']['options']['#description'] = $info['description'];
  }

  $form['settings_container']['options']['#prefix'] = '<div id="webfrom-ticket-submission-class-options">';
  $form['settings_container']['options']['#suffix'] = '</div>';

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );

  return $form;
}

/**
 * Ajax callback: Returns options from settings container.
 */
function webform_ticket_submission_add_service_ajax_callback($form, &$form_state) {
  return $form['settings_container']['options'];
}

/**
 * Validation handler for webform_ticket_submission_settings.
 */
function webform_ticket_submission_settings_validate($form, &$form_state) {

  $service_option = $form_state['values']['options']['service_option'];
  $service_info = webform_ticket_submission_get_services($service_option);

  if (class_exists($service_info['class'])) {
    $service = new $service_info['class']();
  }
  if (!($service instanceof WebformTicketSubmissionInterface)) {
    form_set_error('service_option', t('There seems to be something wrong with the selected service class.'));
    return;
  }
  $form_state['service'] = $service;

  if (!empty($form_state['values']['options']['form'])) {
    $service->configurationFormValidate($form['settings_container']['options']['form'], $form_state['values']['options']['form'], $form_state);
  }
}

/**
 * Submit handler for webform_ticket_submission_settings.
 */
function webform_ticket_submission_settings_submit($form, &$form_state) {

  $form_state['service']->configurationFormSubmit($form['settings_container']['options']['form'], $form_state['values']['options']['form'], $form_state);

  drupal_set_message(t('Your settings have been saved'));

  $nid = $form_state['nid'];
  $values = array(
    'machinename' => $form_state['values']['service_option'],
    'options' => serialize($form_state['values']['options']['form']),
    'disable' => $form_state['values']['settings_disable'],
  );

  if (isset($form_state['values']['service_option'])) {
    // Check to see if the nid in the database exists if so update, else insert.
    db_merge('webform_ticket_submission')
      ->key(array('nid' => $nid))
      ->fields($values)
      ->execute();
  }

}
