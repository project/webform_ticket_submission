<?php

/**
 * @file
 * WebformTicketSubmissionInterface and WebformTicketSubmissionAbstractService.
 */

/**
 * Interface WebformTicketSubmissionInterface.
 *
 * Provides methods which services must implement.
 *
 * These methods are responsible for most of the services connections.
 */
interface WebformTicketSubmissionInterface {

  /**
   * Form constructor for the service configuration form.
   *
   * @param array $form
   *   The current $form values.
   * @param array $form_state
   *   The current form state.
   * @param array $values
   *   The part of the $form_state['values'] array corresponding to this form.
   *
   * @return array
   *   A form array for setting service-specific options.
   */
  public function configurationForm(array $form, array &$form_state, $values);

  /**
   * Validation callback for the form returned by configurationForm().
   *
   * $form_state['service'] will contain the service that is created or edited.
   * Use form_error() to flag errors on form elements.
   *
   * @param array $form
   *   The form returned by configurationForm().
   * @param array $values
   *   The part of the $form_state['values'] array corresponding to this form.
   * @param array $form_state
   *   The complete form state.
   */
  public function configurationFormValidate(array $form, array &$values, array &$form_state);

  /**
   * Submit callback for the form returned by configurationForm().
   *
   * @param array $form
   *   The form returned by configurationForm().
   * @param array $values
   *   The part of the $form_state['values'] array corresponding to this form.
   * @param array $form_state
   *   The complete form state.
   */
  public function configurationFormSubmit(array $form, array &$values, array &$form_state);

  /**
   * Helper function for ticket submission details.
   *
   * @param object $node
   *   Node object of the webform.
   * @param array $input
   *   Data saved in database.
   * @param array $submission
   *   Component field values from submission.
   *
   * @return mixed
   *   Details from execution.
   */
  public function executeDetails($node, array $input, array $submission);

}

/**
 * Class WebformTicketSubmissionAbstractService.
 */
abstract class WebformTicketSubmissionAbstractService implements WebformTicketSubmissionInterface {

  /**
   * Machine name of service.
   *
   * @var object
   */
  protected $service;

  /**
   * Direct reference to the $service option $options property.
   *
   * @var array
   */
  protected $options = array();

  /**
   * The node the webform belongs to.
   *
   * @var string
   */
  public $node;

  /**
   * Implements SearchApiServiceInterface::__construct().
   *
   * Returns an empty form by default.
   */
  public function configurationForm(array $form, array &$form_state, $values) {
    return array();
  }

  /**
   * Does nothing by default.
   */
  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
  }

  /**
   * Additional elements in $options, not present in the form.
   */
  public function configurationFormSubmit(array $form, array &$values, array &$form_state) {
    if (!empty($this->options)) {
      $values += $this->options;
    }
    $this->options = $values;
  }

  /**
   * Returns ticket submission details.
   */
  public function executeDetails($node, array $input, array $submission) {
  }

  /**
   * Renders information regarding the service options. Does nothing by default.
   */
  public function serviceOptionsInfo() {
    return '';
  }

}
